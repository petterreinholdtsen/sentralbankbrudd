# -*- coding: utf-8 -*-
# Copyright (c) 2019 Petter Reinholdtsen <pere@hungry.com>
# This file is covered by the GPLv2 or later, read COPYING for details.

from rest_framework import serializers
from rest_framework_gis.serializers import GeoFeatureModelSerializer

from .models import PrivacyEraser


class PrivacyEraserSerializer(serializers.ModelSerializer):
    class Meta:
        model = PrivacyEraser
        exclude = ('reporter',)


class PrivacyEraserGeoJSONSerializer(GeoFeatureModelSerializer):
    class Meta:
        model = PrivacyEraser
        geo_field = 'location'
        exclude = ('reporter',)
