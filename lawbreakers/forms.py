# -*- coding: utf-8 -*-
# Copyright (c) 2019 Petter Reinholdtsen <pere@hungry.com>
# This file is covered by the GPLv2 or later, read COPYING for details.

import django.forms
from django.contrib.gis import admin

from .admin import PrivacyEraserAdmin
from .models import PrivacyEraser

admin_instance = PrivacyEraserAdmin(PrivacyEraser, admin.site)
location_field = PrivacyEraser._meta.get_field('location')

LocationWidget = admin_instance.get_map_widget(location_field)

class PrivacyEraserForm(django.forms.ModelForm):
    location = django.forms.CharField(widget=LocationWidget(
#        default_lat = 60,
#        default_lon = 10,
    ))
    class Meta:
        model = PrivacyEraser
        exclude = ('reporter',)
