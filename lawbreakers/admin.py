# -*- coding: utf-8 -*-
# Copyright (c) 2018-2019 Petter Reinholdtsen <pere@hungry.com>
# This file is covered by the GPLv2 or later, read COPYING for details.

from django.contrib.gis import admin
from .models import PrivacyEraser

class PrivacyEraserAdmin(admin.OSMGeoAdmin):
    list_filter = ('name', 'reporter','location' )
    list_display = ('location', 'name', 'companyid', 'comment')
admin.site.register(PrivacyEraser, PrivacyEraserAdmin)
