# -*- coding: utf-8 -*-
# Copyright (c) 2018-2019 Petter Reinholdtsen <pere@hungry.com>
# This file is covered by the GPLv2 or later, read COPYING for details.

from django.contrib.auth.models import User
from django.contrib.gis.db import models

class PrivacyEraser(models.Model):
    location = models.PointField()
    name = models.CharField(max_length=50)
    companyid = models.CharField(max_length=50, blank=True, null=True)
    comment = models.TextField(blank=True, null=True)
    reporter = models.ForeignKey(User,
                                 on_delete = models.CASCADE,
#                                 default = current_user,
                                 blank = False, null = False)

    def __str__(self):
        return self.name
