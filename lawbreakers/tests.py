# -*- coding: utf-8 -*-
# Copyright (c) 2018 Petter Reinholdtsen <pere@hungry.com>
# This file is covered by the GPLv2 or later, read COPYING for details.

from django.contrib.auth.models import User
from django.urls import reverse
from django.test import TestCase

class WebsiteTest(TestCase):
    fixtures = ['superuser.json', 'privacyeraser.json']

    username = 'noone@example.com'
    password = 'The hidden secret!'

    def setUp(self):
        """
        Set up test user for be able to log to web site in during testing.
        """

        self.user = User.objects.create_user(self.username, self.username)
        self.user.is_staff = False
        self.user.is_superuser = False
        self.user.firstname = "Jack"
        self.user.lastname = "Shit"
        self.user.set_password(self.password)
        self.user.save()

    def test_frontpage(self):
        response = self.client.get(reverse('lawbreakers-frontpage'))
        self.assertEqual(response.status_code, 200)

    def test_geojson(self):
        response = self.client.get(reverse('lawbreakers-geojson'))
        self.assertEqual(response.status_code, 200)

    def test_new_entry(self):
        response = self.client.post(reverse('lawbreakers-new-lawbreaker'), {
            'location': 'SRID=4326;POINT(1+2)',
            'name': 'Lovbryter',
            'companyid': '1234567890',
            'comment':"Fint",
        })
        print(response)
        self.assertEqual(response.status_code, 200)
