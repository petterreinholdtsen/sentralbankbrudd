# -*- coding: utf-8 -*-
# Copyright (c) 2018 Petter Reinholdtsen <pere@hungry.com>
# This file is covered by the GPLv2 or later, read COPYING for details.

from django.conf import settings


def site_name(request):
    return {
        'SITE_NAME': settings.SITE_NAME,
    }
