Django-app to register shops breaking the Norwegian central bank law
====================================================================

In Norway, it is required by law to accept cash payment.  In addition
to protecting the privacy of customers, it is seen by the central bank
as important to ensure robustness in society.

This django site allow people to report companies and shops breaking
the law by refusing to accept cash payment.

Debian dependencies
-------------------

This program was developed on Debian, and uses the following packages
from the Debian apt repository:

 * python3-django
 * python3-djangorestframework
 * python3-djangorestframework-gis

Testing the site
----------------

To test the site, install all dependencies and run these commands to
get going:

  ./manage.py makemigrations ;
  ./manage.py migrate ;
  ./manage.py collectstatic ;
  ./manage.py runserver ;
